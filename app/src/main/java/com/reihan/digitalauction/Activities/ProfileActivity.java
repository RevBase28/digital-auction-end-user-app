package com.reihan.digitalauction.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.button.MaterialButton;
import com.reihan.digitalauction.Fragments.ChangePasswordBottomSheetFragment;
import com.reihan.digitalauction.Fragments.EditProfileBottomSheetFragment;
import com.reihan.digitalauction.HelperClass.SessionManager;
import com.reihan.digitalauction.HelperClass.TypefaceManager;
import com.reihan.digitalauction.HttpCommunication.ApiClient;
import com.reihan.digitalauction.HttpCommunication.ApiInterface;
import com.reihan.digitalauction.Models.DataModels.UserProfileDataModel;
import com.reihan.digitalauction.Models.ResponseDataModels.ResponseUserProfileModel;
import com.reihan.digitalauction.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfileActivity extends AppCompatActivity {

    private TextView name, username, total_auction, auction_won, total_spent;
    private ImageButton button_back, button_profile_edit;
    private MaterialButton button_logout, button_change_password;

    private TypefaceManager typefaceManager;
    private SessionManager sessionManager;
    private ApiInterface apiInterface;
    private UserProfileDataModel userProfile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        name = findViewById(R.id.name);
        username = findViewById(R.id.username);
        total_auction = findViewById(R.id.total_auction);
        auction_won = findViewById(R.id.auction_won);
        total_spent = findViewById(R.id.total_spent);
        button_back = findViewById(R.id.button_back);
        button_profile_edit = findViewById(R.id.button_edit);
        button_logout = findViewById(R.id.logout_button);
        button_change_password = findViewById(R.id.change_password);

        typefaceManager = new TypefaceManager(this);
        sessionManager = new SessionManager(this);
        apiInterface = ApiClient.getClient().create(ApiInterface.class);

        typefaceManager.setTypeface();

        getUserData(sessionManager.getSessionUserId());

        button_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        button_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showLogoutDialog();
            }
        });

        button_change_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showChangePassBottomSheet(sessionManager.getSessionUserId());
            }
        });

        button_profile_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showEditProfileBottomSheet(userProfile);
            }
        });

    }

    private void getUserData(int id){
        Call<ResponseUserProfileModel> call = apiInterface.getUserProfile(id);

        call.enqueue(new Callback<ResponseUserProfileModel>() {
            @Override
            public void onResponse(Call<ResponseUserProfileModel> call, Response<ResponseUserProfileModel> response) {
                if(response.isSuccessful()){
                    userProfile = response.body().getProfile();

                    name.setText(response.body().getProfile().getNama_lengkap());
                    username.setText(response.body().getProfile().getUsername());
                    total_auction.setText(String.valueOf(response.body().getProfile().getTotal_lelang()));
                    auction_won.setText(String.valueOf(response.body().getProfile().getLelang_won()));
                    total_spent.setText(formatNumber(response.body().getProfile().getTotal_penawaran()));
                }
            }

            @Override
            public void onFailure(Call<ResponseUserProfileModel> call, Throwable t) {
                Log.e("Getting Profile", t.toString() );
                Toast.makeText(getBaseContext(), "Something went wrong", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void showChangePassBottomSheet(int userId){
        ChangePasswordBottomSheetFragment changePassword = new ChangePasswordBottomSheetFragment(userId);

        changePassword.show(getSupportFragmentManager(), "changePassword");
    }

    private void showEditProfileBottomSheet(UserProfileDataModel userProfile){
        EditProfileBottomSheetFragment editProfile = new EditProfileBottomSheetFragment(userProfile, sessionManager.getSessionUserId());

        editProfile.show(getSupportFragmentManager(), "editProfile");
    }

    private void showLogoutDialog(){
        AlertDialog.Builder builder = new AlertDialog.Builder(ProfileActivity.this);

        builder .setTitle("Confirm")
                .setMessage("Do you really want to logout ?")
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        sessionManager.editor.clear();
                        sessionManager.editor.commit();
                        sessionManager.saveSessionLogin(false);

                        startActivity(new Intent(ProfileActivity.this, LoginActivity.class));
                        ProfileActivity.this.finish();
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });

        AlertDialog alertDialog = builder.create();
        alertDialog.show();

        Button positive = alertDialog.getButton(DialogInterface.BUTTON_POSITIVE);
        positive.setTextColor(ProfileActivity.this.getResources().getColor(R.color.colorPrimary));
        positive.setBackgroundColor(ProfileActivity.this.getResources().getColor(R.color.backgroundWhite));

        Button negative = alertDialog.getButton(DialogInterface.BUTTON_NEGATIVE);
        negative.setTextColor(getBaseContext().getResources().getColor(R.color.backgroundWhite));
    }

    private String formatNumber(int number){

        float floatNumber = (float) number;
        String stringNumber = new String();

        if(floatNumber/1000000 > 0){
            stringNumber = String.valueOf(floatNumber/100000) + " M";
        }else if (floatNumber/1000 > 0){
            stringNumber = String.valueOf(floatNumber/1000) + " K";
        }else{
            stringNumber = String.valueOf(number);
        }
        return stringNumber;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        startActivity(new Intent(ProfileActivity.this, MainActivity.class));
        ProfileActivity.this.finish();
    }
}
