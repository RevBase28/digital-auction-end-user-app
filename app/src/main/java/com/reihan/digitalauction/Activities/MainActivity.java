package com.reihan.digitalauction.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.google.android.material.tabs.TabLayout;
import com.google.android.material.textfield.TextInputEditText;
import com.reihan.digitalauction.Adapter.ViewPagerAdapter;
import com.reihan.digitalauction.Fragments.AuctionFragment;
import com.reihan.digitalauction.Fragments.HistoryFragment;
import com.reihan.digitalauction.Fragments.OnBidFragment;
import com.reihan.digitalauction.HelperClass.SessionManager;
import com.reihan.digitalauction.HelperClass.TypefaceManager;
import com.reihan.digitalauction.R;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private TextView name;
    private ImageButton profile;
    private ViewPager viewPager;
    private TabLayout tabLayout;
    private TextInputEditText search_bar;

    private SessionManager sessionManager;
    private TypefaceManager typefaceManager;
    private PagerAdapter pagerAdapter;
    private List<Fragment> fragmentList = new ArrayList<>();
    private List<String> titleList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        profile = findViewById(R.id.profileButton);
        viewPager = findViewById(R.id.viewPager);
        tabLayout = findViewById(R.id.tabLayout);
        name = findViewById(R.id.name);
        search_bar = findViewById(R.id.searchBar);

        populateList();
        sessionManager = new SessionManager(this);
        typefaceManager = new TypefaceManager(this);
        pagerAdapter = new ViewPagerAdapter(getSupportFragmentManager(), R.string.appbar_scrolling_view_behavior, fragmentList, titleList);

        typefaceManager.setTypeface();
        name.setText(sessionManager.getSessionUserName());

        viewPager.setAdapter(pagerAdapter);
        tabLayout.setupWithViewPager(viewPager);

        search_bar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, SearchActivity.class));
                MainActivity.this.finish();
            }
        });

        profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i  = new Intent(MainActivity.this, ProfileActivity.class);
                MainActivity.this.finish();
                startActivity(i);
            }
        });
    }

    public void populateList(){
        fragmentList.add(new AuctionFragment());
        fragmentList.add(new OnBidFragment());
        fragmentList.add(new HistoryFragment());

        titleList.add("Auction");
        titleList.add("On Bid");
        titleList.add("History");
    }
}
