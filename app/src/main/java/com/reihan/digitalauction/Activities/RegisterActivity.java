package com.reihan.digitalauction.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.norbsoft.typefacehelper.TypefaceHelper;
import com.reihan.digitalauction.HelperClass.DelayedProgressDialog;
import com.reihan.digitalauction.HelperClass.ExtendedClikcableSpan;
import com.reihan.digitalauction.HelperClass.TypefaceManager;
import com.reihan.digitalauction.Models.ResponseDataModels.ResponseUserRegisterModel;
import com.reihan.digitalauction.R;
import com.reihan.digitalauction.HttpCommunication.ApiClient;
import com.reihan.digitalauction.HttpCommunication.ApiInterface;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterActivity extends AppCompatActivity {

    private TextView register_to_login;
    private TextInputEditText input_fullname, input_telp, input_username, input_password, input_cfr_password;
    private TextInputLayout layout_fullname, layout_telp, layout_username, layout_password, layout_cfr_password;
    private MaterialButton register_button;

    private ApiInterface apiInterface;
    private TypefaceManager typefaceManager;
    private DelayedProgressDialog dialog = new DelayedProgressDialog();
    private String fullname, username, password, cfr_password, telp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        register_to_login = findViewById(R.id.registerToLogin);
        input_fullname = findViewById(R.id.fullName);
        input_telp = findViewById(R.id.telpNumber);
        input_username = findViewById(R.id.username);
        input_password = findViewById(R.id.password);
        input_cfr_password = findViewById(R.id.cfr_password);
        layout_fullname = findViewById(R.id.layoutFullName);
        layout_username = findViewById(R.id.layoutUsername);
        layout_telp = findViewById(R.id.layoutTelp);
        layout_password = findViewById(R.id.layoutPassword);
        layout_cfr_password = findViewById(R.id.layout_cfr_password);
        register_button = findViewById(R.id.register_button);

        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        typefaceManager = new TypefaceManager(this);
        typefaceManager.setTypeface();
        TypefaceHelper.typeface((View) findViewById(R.id.title1), typefaceManager.fivo_bold_oblique);

        setSpannable();

        register_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fullname = input_fullname.getText().toString();
                username = input_username.getText().toString();
                password = input_password.getText().toString();
                telp = input_telp.getText().toString();
                cfr_password = input_cfr_password.getText().toString();

                Boolean isVerified = verifyInput(fullname, username, password, telp, cfr_password);

                if(isVerified == true){
                    dialog.show(getSupportFragmentManager(), "dialog");
                    dialog.setCancelable(false);

                    registerUser(fullname, username, password, telp);
                }
            }
        });
    }

    public void registerUser(String fullname, String username, String password, String telp ){
        Call<ResponseUserRegisterModel> call = apiInterface.userRegister(fullname, username, password, telp);

        call.enqueue(new Callback<ResponseUserRegisterModel>() {
            @Override
            public void onResponse(Call<ResponseUserRegisterModel> call, Response<ResponseUserRegisterModel> response) {
                if(response.isSuccessful()){
                    dialog.dismiss();

                    Toast.makeText(getBaseContext(), "User sucessfully registered", Toast.LENGTH_SHORT).show();

                    startActivity(new Intent(RegisterActivity.this, LoginActivity.class));
                    RegisterActivity.this.finish();
                }
            }

            @Override
            public void onFailure(Call<ResponseUserRegisterModel> call, Throwable t) {
                dialog.dismiss();
                Toast.makeText(getBaseContext(), "Something went wrong", Toast.LENGTH_SHORT).show();

                Log.e("Error register", t.toString());
            }
        });
    }

    public Boolean verifyInput(String fullname, String username, String password, String telp, String cfr_password){
        if(fullname.equals("") || username.equals("") || password.equals("") || telp.equals("") || cfr_password.equals("")) {
            if (fullname.equals("")) {
                layout_fullname.setError("Fullname must be filled");
            }
            if (username.equals("")) {
                layout_username.setError("Username must be filled");
            }
            if (password.equals("")) {
                layout_password.setError(getResources().getString(R.string.min_pass_char));
            }
            if (telp.equals("")) {
                layout_telp.setError("Telephone Number must be filled");
            }
            if (cfr_password.equals("")) {
                layout_cfr_password.setError("Type again your password");
            }

            return false;
        }
        else if (password.length() < 6 || !password.equals(cfr_password)){
            if(password.length() < 6){
                layout_password.setError(getResources().getString(R.string.min_pass_char));
            }
            if(!password.equals(cfr_password)){
                layout_cfr_password.setError("Password don't match");
            }

            return false;
        }
        else{
            return true;
        }
    }

    public void setSpannable(){
        int color = getResources().getColor(R.color.colorPrimary);

        SpannableString toLogin = new SpannableString(getString(R.string.register_to_login));
        toLogin.setSpan(new ForegroundColorSpan(color), 23, toLogin.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        toLogin.setSpan(new ExtendedClikcableSpan(getString(R.string.register_to_login),getBaseContext(),LoginActivity.class),23,toLogin.length(),Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        register_to_login.setText(toLogin);
        register_to_login.setMovementMethod(LinkMovementMethod.getInstance());
    }
}
