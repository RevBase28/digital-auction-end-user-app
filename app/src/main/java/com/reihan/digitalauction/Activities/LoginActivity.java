package com.reihan.digitalauction.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.norbsoft.typefacehelper.TypefaceHelper;
import com.reihan.digitalauction.HelperClass.DelayedProgressDialog;
import com.reihan.digitalauction.HelperClass.ExtendedClikcableSpan;
import com.reihan.digitalauction.HelperClass.SessionManager;
import com.reihan.digitalauction.HelperClass.TypefaceManager;
import com.reihan.digitalauction.Models.ResponseDataModels.ResponseUserLoginModel;
import com.reihan.digitalauction.R;
import com.reihan.digitalauction.HttpCommunication.ApiClient;
import com.reihan.digitalauction.HttpCommunication.ApiInterface;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {

    private TextView login_to_register_tv,text_warning;
    private TextInputEditText input_username, input_password;
    private TextInputLayout layout_username, layout_password;
    private MaterialButton login_button;

    private ApiInterface apiInterface;
    private TypefaceManager typefaceManager;
    private DelayedProgressDialog dialog = new DelayedProgressDialog();
    private String username, password;
    private SessionManager sessionManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        login_to_register_tv = findViewById(R.id.loginToRegister);
        text_warning = findViewById(R.id.textWarning);
        input_password = findViewById(R.id.password);
        input_username = findViewById(R.id.username);
        layout_password = findViewById(R.id.layoutPassword);
        layout_username = findViewById(R.id.layoutUsername);
        login_button = findViewById(R.id.loginButton);

        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        sessionManager = new SessionManager(LoginActivity.this);
        typefaceManager = new TypefaceManager(this);

        typefaceManager.setTypeface();
        TypefaceHelper.typeface((View) findViewById(R.id.title1), typefaceManager.fivo_bold_oblique);
        setSpannable();
        checkSession(sessionManager.getSessionLogin());


        login_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                username = input_username.getText().toString();
                password = input_password.getText().toString();

                if(username.equals("") || password.equals("")){
                    if(username.equals("")){
                        layout_username.setError("Username must be filled");
                    }
                    if(password.equals("")){
                        layout_password.setError("Password must be filled");
                    }
                }else{
                    dialog.show(getSupportFragmentManager(), "dialog");
                    dialog.setCancelable(false);

                    userLogin(username, password);
                }

            }
        });
    }

    public void userLogin(String username, String password){
        Toast.makeText(getBaseContext(), "User Login", Toast.LENGTH_SHORT).show();
        Call<ResponseUserLoginModel> call = apiInterface.userLogin(username, password);

        call.enqueue(new Callback<ResponseUserLoginModel>() {
            @Override
            public void onResponse(Call<ResponseUserLoginModel> call, Response<ResponseUserLoginModel> response) {
                dialog.dismiss();

                if(response.isSuccessful()){
                    sessionManager.saveSessionLogin(true);
                    sessionManager.saveSessionUserId(response.body().getUser().getId());
                    sessionManager.saveSessionUserName(response.body().getUser().getNama_lengkap());

                    startActivity(new Intent(LoginActivity.this, MainActivity.class));
                    LoginActivity.this.finish();

                }else{
                    text_warning.setText("Wrong username or password");
                    text_warning.setVisibility(View.VISIBLE);

                    input_password.setText("");
                }
            }

            @Override
            public void onFailure(Call<ResponseUserLoginModel> call, Throwable t) {
                dialog.dismiss();

                text_warning.setText("Something went wrong");
                text_warning.setVisibility(View.VISIBLE);

                Log.e("Login Error", t.toString());
            }
        });
    }

    public void checkSession(Boolean login){
        if(login == true){
            startActivity(new Intent(LoginActivity.this, MainActivity.class));
            LoginActivity.this.finish();
        }
    }

    public void setSpannable(){
        int color = getResources().getColor(R.color.colorPrimary);

        SpannableString toRegister = new SpannableString(getString(R.string.login_to_register));
        toRegister.setSpan(new ForegroundColorSpan(color), 24, toRegister.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        toRegister.setSpan(new ExtendedClikcableSpan(getString(R.string.login_to_register),getBaseContext(),RegisterActivity.class),24,toRegister.length(),Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        login_to_register_tv.setText(toRegister);
        login_to_register_tv.setMovementMethod(LinkMovementMethod.getInstance());
    }
}
