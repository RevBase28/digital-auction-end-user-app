package com.reihan.digitalauction.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.github.ybq.android.spinkit.SpinKitView;
import com.google.android.material.textfield.TextInputEditText;
import com.reihan.digitalauction.Adapter.AuctionRecyclerAdapter;
import com.reihan.digitalauction.HelperClass.TypefaceManager;
import com.reihan.digitalauction.HttpCommunication.ApiClient;
import com.reihan.digitalauction.HttpCommunication.ApiInterface;
import com.reihan.digitalauction.Models.DataModels.AuctionActiveDataModel;
import com.reihan.digitalauction.Models.ResponseDataModels.ResponseAuctionSearchModel;
import com.reihan.digitalauction.R;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchActivity extends AppCompatActivity {

    private ImageButton button_back;
    private TextInputEditText search_bar;
    private RecyclerView recyclerView;
    private SpinKitView progress;
    private LinearLayout layoutNotFound;
    private TextView notFoundText;
    private RecyclerView.LayoutManager layoutManager;

    private TypefaceManager typefaceManager;
    private ApiInterface apiInterface;
    private List<AuctionActiveDataModel> results = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        button_back = findViewById(R.id.buttonBack);
        search_bar = findViewById(R.id.searchBar);
        recyclerView = findViewById(R.id.lelangRecycler);
        progress = findViewById(R.id.progress);
        layoutNotFound = findViewById(R.id.layoutNotFound);
        notFoundText = findViewById(R.id.not_found_text);

        layoutManager = new LinearLayoutManager(this);
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        typefaceManager = new TypefaceManager(this);
        typefaceManager.setTypeface();

        recyclerView.setLayoutManager(layoutManager);

        recyclerView.setVisibility(View.INVISIBLE);
        progress.setVisibility(View.INVISIBLE);
        layoutNotFound.setVisibility(View.GONE);

        button_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        search_bar.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
                if(actionId == EditorInfo.IME_ACTION_SEARCH){
                    progress.setVisibility(View.VISIBLE);

                    String query = search_bar.getText().toString();

                    searchAuction(query);
                    return true;
                }

                return false;
            }
        });

    }

    private void searchAuction(String query){
        Call<ResponseAuctionSearchModel> call = apiInterface.searchAuction(query);

        call.enqueue(new Callback<ResponseAuctionSearchModel>() {
            @Override
            public void onResponse(Call<ResponseAuctionSearchModel> call, Response<ResponseAuctionSearchModel> response) {
                if(response.isSuccessful()){
                    results = response.body().getResults();

                    recyclerView.setAdapter(new AuctionRecyclerAdapter(results, SearchActivity.this, R.layout.item_recycler_auction));
                    progress.setVisibility(View.INVISIBLE);
                    if(results.size() > 0){
                        recyclerView.setVisibility(View.VISIBLE);
                    }else{
                        layoutNotFound.setVisibility(View.VISIBLE);
                        notFoundText.setText("No result for '" + query + "'");
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseAuctionSearchModel> call, Throwable t) {
                progress.setVisibility(View.INVISIBLE);
                Log.e("Search Error", t.toString());
                Toast.makeText(getBaseContext(), "Something went wrong", Toast.LENGTH_SHORT).show();
            }
        });
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();

        startActivity(new Intent(SearchActivity.this, MainActivity.class));
        SearchActivity.this.finish();
    }
}
