package com.reihan.digitalauction.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.github.ybq.android.spinkit.SpinKitView;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.android.material.button.MaterialButton;
import com.reihan.digitalauction.Adapter.ImageSliderAdapter;
import com.reihan.digitalauction.Fragments.BidBottomSheetFragment;
import com.reihan.digitalauction.HelperClass.SessionManager;
import com.reihan.digitalauction.HelperClass.TypefaceManager;
import com.reihan.digitalauction.HttpCommunication.ApiClient;
import com.reihan.digitalauction.HttpCommunication.ApiInterface;
import com.reihan.digitalauction.Models.ResponseDataModels.ResponseAuctionDetailModel;
import com.reihan.digitalauction.R;
import com.smarteist.autoimageslider.IndicatorAnimations;
import com.smarteist.autoimageslider.SliderAnimations;
import com.smarteist.autoimageslider.SliderView;

import java.sql.Timestamp;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AuctionDetailActivity extends AppCompatActivity {

    private SliderView imageSlider;
    private TextView countdown, highestPrice, item_name, item_desc, custodian_name, starting_price, bidder_name, last_bid_time, bid_price;
    private CardView card_bidder;
    private SpinKitView progress;
    private MaterialButton place_a_bid;
    private ConstraintLayout container;

    private ApiInterface apiInterface;
    private TypefaceManager typefaceManager;
    private int id, highest_price;
    private SessionManager sessionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auction_detail);

        imageSlider = findViewById(R.id.imageSlider);
        countdown = findViewById(R.id.countdown);
        highestPrice = findViewById(R.id.highestPrice);
        item_name = findViewById(R.id.itemName);
        item_desc = findViewById(R.id.itemDesc);
        custodian_name = findViewById(R.id.custodianName);
        starting_price = findViewById(R.id.startingPrice);
        bidder_name = findViewById(R.id.bidderName);
        last_bid_time = findViewById(R.id.lastTimeBid);
        bid_price = findViewById(R.id.bidPrice);
        card_bidder = findViewById(R.id.cardBidder);
        progress = findViewById(R.id.progress);
        place_a_bid = findViewById(R.id.placeABid);
        container = findViewById(R.id.contentContainer);

        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        typefaceManager = new TypefaceManager(this);
        sessionManager = new SessionManager(this);
        id = getIntent().getIntExtra("id", 0);

        typefaceManager.setTypeface();

        container.setVisibility(View.INVISIBLE);
        progress.setVisibility(View.VISIBLE);

        getAuctionDetail(id);

        place_a_bid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showBidBottomSheet();
            }
        });
    }

    public void showBidBottomSheet(){
        BidBottomSheetFragment bottomSheet = new BidBottomSheetFragment(highest_price, id);

        bottomSheet.show(getSupportFragmentManager(), "bottomSheet");
    }

    public void getAuctionDetail(int id){
        Call<ResponseAuctionDetailModel> call = apiInterface.getDetailAuction(id);

        call.enqueue(new Callback<ResponseAuctionDetailModel>() {
            @Override
            public void onResponse(Call<ResponseAuctionDetailModel> call, Response<ResponseAuctionDetailModel> response) {
                imageSlider.setSliderAdapter(new ImageSliderAdapter(AuctionDetailActivity.this, response.body().getLelang().getBarang().getPhotos()));
                imageSlider.setScrollTimeInSec(3);
                imageSlider.startAutoCycle();
                imageSlider.setIndicatorAnimation(IndicatorAnimations.WORM);
                imageSlider.setSliderTransformAnimation(SliderAnimations.SIMPLETRANSFORMATION);

                highest_price = response.body().getLelang().getHarga_akhir();
                setCountdown(response.body().getLelang().getEnd_lelang(), response.body().getLelang().getUpdated_at());
                highestPrice.setText("Rp. " + String.valueOf(response.body().getLelang().getHarga_akhir()));
                item_name.setText(response.body().getLelang().getBarang().getNama_barang());
                item_desc.setText(response.body().getLelang().getBarang().getDeskripsi_barang());
                custodian_name.setText(response.body().getLelang().getPetugas().getNama_petugas());
                starting_price.setText("Rp. " + String.valueOf(response.body().getLelang().getBarang().getHarga_awal()));

                if (response.body().getLelang().getBidder() == null){
                    card_bidder.setVisibility(View.GONE);
                }else{
                    bidder_name.setText(response.body().getLelang().getBidder().getNama_lengkap());
                    bid_price.setText("Rp. " + String.valueOf(response.body().getLelang().getHarga_akhir()));
                }

                try{
                    if(response.body().getLelang().getBidder().getId() == sessionManager.getSessionUserId()){
                        place_a_bid.setEnabled(false);
                    }
                }catch (NullPointerException e){
                    place_a_bid.setEnabled(true);
                }

                container.setVisibility(View.VISIBLE);
                progress.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<ResponseAuctionDetailModel> call, Throwable t) {
                Log.e("Failed Detail", t.toString());
            }
        });
    }

    public void setCountdown(String endAuction, String updatedAt) {
        long startAuctionMillis,endAuctionMillis,totalMillis;

        if(endAuction != null){
            Timestamp endAuctionTimestamp = Timestamp.valueOf(endAuction);
            startAuctionMillis = System.currentTimeMillis();
            endAuctionMillis = endAuctionTimestamp.getTime();
            totalMillis = endAuctionMillis - startAuctionMillis;

            CountDownTimer countDownTimer = new CountDownTimer(totalMillis, 1000) {
                @Override
                public void onTick(long timeLeftMillis) {
                    long second = (timeLeftMillis/ 1000) % 60;
                    long minutes =((timeLeftMillis/ (1000*60)) % 60);
                    long hours =((timeLeftMillis / (1000*60*60)) % 24);
                    long days = ((timeLeftMillis / (1000*60*60*24)) % 24);

                    if(days > 0){
                        countdown.setText(String.valueOf(days) + " Days "+ String.format("%02dh %02dm",hours,minutes));
                    }else{
                        countdown.setText(String.format("%02dh %02dm %02ds",hours,minutes,second));
                    }

                    if(card_bidder.getVisibility() == View.VISIBLE){
                        setLastTimeBid(updatedAt);
                    }
                }

                @Override
                public void onFinish() {
                    countdown.setText("Finish");
                    place_a_bid.setEnabled(false);

                    startActivity(new Intent(AuctionDetailActivity.this, MainActivity.class));
                    AuctionDetailActivity.this.finish();
                }

            }.start();

        }else{
            countdown.setText("Indefinetly");
        }

    }

    public void setLastTimeBid(String updatedAt){

        long currentMillis,updatedMillis,totalMillis;


        Timestamp updatedAtTimestamp = Timestamp.valueOf(updatedAt);
        currentMillis = System.currentTimeMillis();
        updatedMillis = updatedAtTimestamp.getTime();
        totalMillis = currentMillis - updatedMillis;

        long second = (totalMillis/ 1000) % 60;
        long minutes =((totalMillis/ (1000*60)) % 60);
        long hours =((totalMillis/ (1000*60*60)) % 24);
        long days = ((totalMillis / (1000*60*60*24)) % 24);

        if(days > 0){
            last_bid_time.setText(String.valueOf(days) + " days ago");
        }else if(hours > 0){
            last_bid_time.setText(String.valueOf(hours) + " hours ago");
        }else if(minutes > 0){
            last_bid_time.setText(String.valueOf(minutes) + " minutes ago");
        }else{
            last_bid_time.setText(String.valueOf(second) + " second ago");
        }
    }
}
