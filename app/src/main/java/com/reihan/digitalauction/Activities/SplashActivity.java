package com.reihan.digitalauction.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.TextView;

import com.norbsoft.typefacehelper.TypefaceHelper;
import com.reihan.digitalauction.HelperClass.TypefaceManager;
import com.reihan.digitalauction.R;

public class SplashActivity extends AppCompatActivity {

    private TypefaceManager typefaceManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        typefaceManager = new TypefaceManager(this);

        typefaceManager.setTypeface();

        TypefaceHelper.typeface((View) findViewById(R.id.title1),typefaceManager.fivo_bold_oblique);
        TypefaceHelper.typeface((View) findViewById(R.id.title2), typefaceManager.fivo_bold_oblique);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(new Intent(SplashActivity.this, LoginActivity.class));
                SplashActivity.this.finish();
            }
        }, 3000);
    }
}
