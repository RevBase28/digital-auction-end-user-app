package com.reihan.digitalauction.Fragments;


import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.norbsoft.typefacehelper.TypefaceHelper;
import com.reihan.digitalauction.Activities.MainActivity;
import com.reihan.digitalauction.HelperClass.DelayedProgressDialog;
import com.reihan.digitalauction.HelperClass.SessionManager;
import com.reihan.digitalauction.HelperClass.TypefaceManager;
import com.reihan.digitalauction.HttpCommunication.ApiClient;
import com.reihan.digitalauction.HttpCommunication.ApiInterface;
import com.reihan.digitalauction.Models.DataModels.UserProfileDataModel;
import com.reihan.digitalauction.Models.ResponseDataModels.ResponseUserProfileUpdateModel;
import com.reihan.digitalauction.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class EditProfileBottomSheetFragment extends BottomSheetDialogFragment {

    private TextInputEditText full_name, username, telp;
    private TextInputLayout layout_fullname, layout_username, layout_telp;
    private MaterialButton update_profile;

    private TypefaceManager typefaceManager;
    private SessionManager sessionManager;
    private ApiInterface apiInterface;
    private DelayedProgressDialog dialog = new DelayedProgressDialog();
    private UserProfileDataModel userProfile;
    private int userId;
    private String value_fullname, value_username, value_telp;

    public EditProfileBottomSheetFragment(UserProfileDataModel userProfile, int userId) {
        this.userProfile = userProfile;
        this.userId = userId;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_edit_profile_bottom_sheet, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        full_name = view.findViewById(R.id.fullName);
        username = view.findViewById(R.id.username);
        telp = view.findViewById(R.id.telp);
        layout_fullname = view.findViewById(R.id.layoutFullName);
        layout_username = view.findViewById(R.id.layoutUsername);
        layout_telp = view.findViewById(R.id.layoutTelp);
        update_profile = view.findViewById(R.id.updateProfile);

        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        sessionManager = new SessionManager(view.getContext());
        typefaceManager = new TypefaceManager(getActivity());
        typefaceManager.setTypeface();
        setFragmentTypeface(view);

        full_name.setText(userProfile.getNama_lengkap());
        username.setText(userProfile.getUsername());
        telp.setText(userProfile.getTelp());

        update_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                value_fullname = full_name.getText().toString();
                value_username = username.getText().toString();
                value_telp = telp.getText().toString();

                Boolean isVerified = verifyInput(value_fullname, value_username, value_telp);

                if(isVerified == true){
                    dialog.show(getChildFragmentManager(), "dialog");
                    dialog.setCancelable(false);

                    updateProfile(value_fullname, value_username, value_telp, view);
                }
            }
        });

    }

    public Boolean verifyInput(String fullname, String username, String telp){

        if(fullname.equals("") || username.equals("") || telp.equals("")) {
            if (fullname.equals("")) {
                layout_fullname.setError("Fullname must be filled");
            }
            if (username.equals("")) {
                layout_username.setError("Username must be filled");
            }

            if (telp.equals("")) {
                layout_telp.setError("Telephone Number must be filled");
            }

            return false;
        }

        else{
            return true;
        }
    }

    public void updateProfile(String fullName, String username, String telp, View view){
        Call<ResponseUserProfileUpdateModel> call = apiInterface.updateProfile(userId, fullName, username, telp);

        call.enqueue(new Callback<ResponseUserProfileUpdateModel>() {
            @Override
            public void onResponse(Call<ResponseUserProfileUpdateModel> call, Response<ResponseUserProfileUpdateModel> response) {
                if(response.isSuccessful()){
                    dialog.dismiss();
                    Toast.makeText(view.getContext(), "Profile Sucessfully updated", Toast.LENGTH_SHORT).show();

                    sessionManager.saveSessionUserName(response.body().getProfile().getNama_lengkap());

                    startActivity(new Intent(getActivity(), MainActivity.class));
                    getActivity().finish();
                }
            }

            @Override
            public void onFailure(Call<ResponseUserProfileUpdateModel> call, Throwable t) {
                Log.e("Update Profile", t.toString());
                dialog.dismiss();

                layout_username.setError("Username probably already used");
                layout_fullname.setError(null);
                layout_telp.setError(null);
            }
        });
    }

    public void setFragmentTypeface(View view){
        TypefaceHelper.typeface((View) view.findViewById(R.id.textView4), typefaceManager.fivo);
        TypefaceHelper.typeface((View) view.findViewById(R.id.fullName), typefaceManager.fivo);
        TypefaceHelper.typeface((View) view.findViewById(R.id.username), typefaceManager.fivo);
        TypefaceHelper.typeface((View) view.findViewById(R.id.telp), typefaceManager.fivo);
        TypefaceHelper.typeface((View) view.findViewById(R.id.updateProfile), typefaceManager.fivo);
        TypefaceHelper.typeface((View) view.findViewById(R.id.layoutFullName), typefaceManager.fivo);
        TypefaceHelper.typeface((View) view.findViewById(R.id.layoutUsername), typefaceManager.fivo);
        TypefaceHelper.typeface((View) view.findViewById(R.id.layoutTelp), typefaceManager.fivo);
    }
}
