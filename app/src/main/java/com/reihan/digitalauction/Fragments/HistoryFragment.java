package com.reihan.digitalauction.Fragments;


import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.github.ybq.android.spinkit.SpinKitView;
import com.norbsoft.typefacehelper.TypefaceHelper;
import com.reihan.digitalauction.Adapter.AuctionHistoryRecyclerAdapter;
import com.reihan.digitalauction.Adapter.AuctionRecyclerAdapter;
import com.reihan.digitalauction.HelperClass.SessionManager;
import com.reihan.digitalauction.HelperClass.TypefaceManager;
import com.reihan.digitalauction.HttpCommunication.ApiClient;
import com.reihan.digitalauction.HttpCommunication.ApiInterface;
import com.reihan.digitalauction.Models.DataModels.UsersAuctionHistoryDataModel;
import com.reihan.digitalauction.Models.ResponseDataModels.ResponseUsersAuctionHistoryModel;
import com.reihan.digitalauction.R;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class HistoryFragment extends Fragment {

    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private SpinKitView progress;
    private LinearLayout layoutNoData;
    private TextView noDataText;

    private ApiInterface apiInterface;
    private List<UsersAuctionHistoryDataModel> historyData;
    private SessionManager sessionManager;
    private TypefaceManager typefaceManager;

    public HistoryFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_history, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        recyclerView = view.findViewById(R.id.lelangRecycler);
        progress = view.findViewById(R.id.progress);
        layoutManager = new LinearLayoutManager(view.getContext());
        layoutNoData = view.findViewById(R.id.layoutNoData);
        noDataText = view.findViewById(R.id.not_data_text);

        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        sessionManager = new SessionManager(view.getContext());
        typefaceManager = new TypefaceManager(getActivity());

        typefaceManager.setTypeface();
        TypefaceHelper.typeface(noDataText, typefaceManager.fivo);

        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setVisibility(View.INVISIBLE);
        progress.setVisibility(View.VISIBLE);
        layoutNoData.setVisibility(View.GONE);

        getUsersHistory(sessionManager.getSessionUserId(), view);
    }

    public void getUsersHistory(int id, View view){
        Call<ResponseUsersAuctionHistoryModel> call = apiInterface.getUsersHistory(id);

        call.enqueue(new Callback<ResponseUsersAuctionHistoryModel>() {
            @Override
            public void onResponse(Call<ResponseUsersAuctionHistoryModel> call, Response<ResponseUsersAuctionHistoryModel> response) {
                if(response.isSuccessful()){
                    historyData = response.body().getHistory();

                    if(historyData.size() == 0){
                        layoutNoData.setVisibility(View.VISIBLE);
                    }

                    recyclerView.setAdapter(new AuctionHistoryRecyclerAdapter(historyData, view.getContext(), R.layout.item_recycler_history ));

                    recyclerView.setVisibility(View.VISIBLE);
                    progress.setVisibility(View.INVISIBLE);
                }
            }

            @Override
            public void onFailure(Call<ResponseUsersAuctionHistoryModel> call, Throwable t) {
                Log.e("AuctionHistory", "Error getting data : " + t.toString());

                progress.setVisibility(View.INVISIBLE);
                Toast.makeText(view.getContext(), "Something went wrong", Toast.LENGTH_SHORT);
            }
        });
    }
}
