package com.reihan.digitalauction.Fragments;


import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.norbsoft.typefacehelper.TypefaceHelper;
import com.reihan.digitalauction.Activities.MainActivity;
import com.reihan.digitalauction.HelperClass.DelayedProgressDialog;
import com.reihan.digitalauction.HelperClass.SessionManager;
import com.reihan.digitalauction.HelperClass.TypefaceManager;
import com.reihan.digitalauction.HttpCommunication.ApiClient;
import com.reihan.digitalauction.HttpCommunication.ApiInterface;
import com.reihan.digitalauction.Models.ResponseDataModels.ResponseAddOfferModel;
import com.reihan.digitalauction.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BidBottomSheetFragment extends BottomSheetDialogFragment {

    private TextView highest_price;
    private TextInputEditText input_bid_price;
    private TextInputLayout layout_input_bid_price;
    private MaterialButton button_add_bid;

    private ApiInterface apiInterface;
    private SessionManager sessionManager;
    private TypefaceManager typefaceManager;
    private DelayedProgressDialog dialog = new DelayedProgressDialog();
    private int highestPrice = 0, itemId, bidPrice;

    public BidBottomSheetFragment(int highestPrice, int itemId) {
        this.highestPrice = highestPrice;
        this.itemId = itemId;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_bottom_fragment_dialog, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        highest_price = view.findViewById(R.id.highestPrice);
        input_bid_price = view.findViewById(R.id.bidPrice);
        layout_input_bid_price = view.findViewById(R.id.layoutBidPrice);
        button_add_bid = view.findViewById(R.id.placeABid);

        typefaceManager = new TypefaceManager(getActivity());
        typefaceManager.setTypeface();
        sessionManager = new SessionManager(view.getContext());
        apiInterface = ApiClient.getClient().create(ApiInterface.class);

        highest_price.setText("Rp. " + String.valueOf(highestPrice));

        button_add_bid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bidPrice = Integer.valueOf(input_bid_price.getText().toString());

                if(bidPrice < highestPrice){
                    layout_input_bid_price.setError("Your Bid is to low");
                }else{
                    dialog.show(getChildFragmentManager(), "dialog");
                    dialog.setCancelable(false);
                    addOffer(itemId, bidPrice, view);
                }
            }
        });

        setFragmentTypeface(view);
    }

    public void addOffer(int id, int bidPrice, View view){
        Call<ResponseAddOfferModel> call = apiInterface.addOffer(id, bidPrice, sessionManager.getSessionUserId());

        call.enqueue(new Callback<ResponseAddOfferModel>() {
            @Override
            public void onResponse(Call<ResponseAddOfferModel> call, Response<ResponseAddOfferModel> response) {
                dialog.dismiss();

                if(response.isSuccessful()) {
                    if(response.body().getCode() == 201){
                        Toast.makeText(view.getContext() , "Bid Accepted", Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(getActivity(), MainActivity.class));
                        getActivity().finish();
                    }else{
                        Toast.makeText(view.getContext() , "Bid Declined", Toast.LENGTH_SHORT).show();
                        highest_price.setText("Rp. " + String.valueOf(response.body().getLelang().getHarga_akhir()));
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseAddOfferModel> call, Throwable t) {
                dialog.dismiss();
                Toast.makeText(view.getContext() , "Something went wrong", Toast.LENGTH_SHORT).show();
                Log.e("Bid Error", t.toString());
            }
        });
    }

    public void setFragmentTypeface(View view){
        TypefaceHelper.typeface((View) view.findViewById(R.id.textView4), typefaceManager.fivo);
        TypefaceHelper.typeface((View) view.findViewById(R.id.textView3), typefaceManager.fivo);
        TypefaceHelper.typeface((View) view.findViewById(R.id.highestPrice), typefaceManager.fivo);
        TypefaceHelper.typeface((View) view.findViewById(R.id.layoutBidPrice), typefaceManager.fivo);
        TypefaceHelper.typeface((View) view.findViewById(R.id.bidPrice), typefaceManager.fivo);
        TypefaceHelper.typeface((View) view.findViewById(R.id.placeABid), typefaceManager.fivo);
    }


}
