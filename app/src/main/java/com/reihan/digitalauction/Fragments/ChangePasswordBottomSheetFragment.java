package com.reihan.digitalauction.Fragments;


import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.norbsoft.typefacehelper.TypefaceHelper;
import com.reihan.digitalauction.Activities.LoginActivity;
import com.reihan.digitalauction.Activities.MainActivity;
import com.reihan.digitalauction.HelperClass.DelayedProgressDialog;
import com.reihan.digitalauction.HelperClass.SessionManager;
import com.reihan.digitalauction.HelperClass.TypefaceManager;
import com.reihan.digitalauction.HttpCommunication.ApiClient;
import com.reihan.digitalauction.HttpCommunication.ApiInterface;
import com.reihan.digitalauction.Models.ResponseDataModels.ResponseChangePasswordModel;
import com.reihan.digitalauction.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class ChangePasswordBottomSheetFragment extends BottomSheetDialogFragment {

    private TextInputEditText old_password, new_password, cfr_password;
    private TextInputLayout layout_old_password, layout_new_password, layout_cfr_password;
    private MaterialButton change_password;

    private int id;
    private String value_old_password, value_new_password, value_cfr_password;
    private TypefaceManager typefaceManager;
    private ApiInterface apiInterface;
    private SessionManager sessionManager;
    private DelayedProgressDialog dialog = new DelayedProgressDialog();

    public ChangePasswordBottomSheetFragment(int id) {
        this.id = id;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_change_password_bottom_sheet, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        old_password = view.findViewById(R.id.oldPassword);
        new_password = view.findViewById(R.id.password);
        cfr_password = view.findViewById(R.id.cfr_password);
        layout_old_password = view.findViewById(R.id.layoutOldPassword);
        layout_new_password = view.findViewById(R.id.layoutPassword);
        layout_cfr_password = view.findViewById(R.id.layout_cfr_password);
        change_password = view.findViewById(R.id.changePassword);

        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        sessionManager = new SessionManager(view.getContext());
        typefaceManager = new TypefaceManager(getActivity());
        typefaceManager.setTypeface();
        setFragmentTypeface(view);

        change_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                value_old_password = old_password.getText().toString();
                value_new_password = new_password.getText().toString();
                value_cfr_password = cfr_password.getText().toString();

                Boolean isVerified = verifyInput(value_old_password, value_new_password, value_cfr_password);

                if(isVerified == true){
                    dialog.show(getChildFragmentManager(), "dialog");
                    dialog.setCancelable(false);

                    changePassword(sessionManager.getSessionUserId(), value_old_password, value_new_password, view);
                }
            }
        });
    }

    public void changePassword(int id, String oldPassword, String newPassword, View view){
        Call<ResponseChangePasswordModel> call = apiInterface.changePassword(id, oldPassword, newPassword);

        call.enqueue(new Callback<ResponseChangePasswordModel>() {
            @Override
            public void onResponse(Call<ResponseChangePasswordModel> call, Response<ResponseChangePasswordModel> response) {
                dialog.dismiss();

                if(response.isSuccessful()){
                    Toast.makeText(view.getContext(), "Password Sucessfully Changed", Toast.LENGTH_SHORT).show();

                    sessionManager.editor.clear();
                    sessionManager.editor.commit();
                    sessionManager.saveSessionLogin(false);

                    startActivity(new Intent(getActivity(), LoginActivity.class));
                    getActivity().finish();
                }
            }

            @Override
            public void onFailure(Call<ResponseChangePasswordModel> call, Throwable t) {
                Log.e("Change Pass", t.toString());
                dialog.dismiss();

                old_password.setText("");
                new_password.setText("");
                cfr_password.setText("");

                layout_old_password.setError("Check again your old password");
                layout_new_password.setError(null);
                layout_cfr_password.setError(null);
            }
        });
    }

    public void setFragmentTypeface(View view){
        TypefaceHelper.typeface((View) view.findViewById(R.id.textView4), typefaceManager.fivo);
        TypefaceHelper.typeface((View) view.findViewById(R.id.layoutOldPassword), typefaceManager.fivo);
        TypefaceHelper.typeface((View) view.findViewById(R.id.layoutPassword), typefaceManager.fivo);
        TypefaceHelper.typeface((View) view.findViewById(R.id.layout_cfr_password), typefaceManager.fivo);
        TypefaceHelper.typeface((View) view.findViewById(R.id.oldPassword), typefaceManager.fivo);
        TypefaceHelper.typeface((View) view.findViewById(R.id.password), typefaceManager.fivo);
        TypefaceHelper.typeface((View) view.findViewById(R.id.cfr_password), typefaceManager.fivo);
        TypefaceHelper.typeface((View) view.findViewById(R.id.changePassword), typefaceManager.fivo);
    }

    public Boolean verifyInput(String value_old_password, String value_new_password, String value_cfr_password){

        if(value_old_password.equals("") || value_new_password.equals("") || value_cfr_password.equals("")) {
            if (value_old_password.equals("")) {
                layout_old_password.setError("Old Password must be filled");
            }
            if (value_new_password.equals("")) {
                layout_new_password.setError("New Password must be filled");
            }
            if (value_cfr_password.equals("")) {
                layout_cfr_password.setError("Type again your password");
            }

            return false;
        }
        else if (value_new_password.length() < 6 || !value_new_password.equals(value_cfr_password)){
            if(value_new_password.length() < 6){
                layout_new_password.setError(getResources().getString(R.string.min_pass_char));
            }
            if(!value_new_password.equals(value_cfr_password)){
                layout_cfr_password.setError("Password don't match");
            }

            return false;
        }
        else{
            return true;
        }

    }
}
