package com.reihan.digitalauction.Models.DataModels;

public class CustodianDataModel {

    private int id_petugas;
    private String nama_petugas;
    private String username;
    private int id_level;
    private String created_at;
    private String update_at;
    private int jumlah_lelang;

    public CustodianDataModel(int id_petugas, String nama_petugas, String username, int id_level, String created_at, String update_at, int jumlah_lelang) {
        this.id_petugas = id_petugas;
        this.nama_petugas = nama_petugas;
        this.username = username;
        this.id_level = id_level;
        this.created_at = created_at;
        this.update_at = update_at;
        this.jumlah_lelang = jumlah_lelang;
    }

    public int getId_petugas() {
        return id_petugas;
    }

    public void setId_petugas(int id_petugas) {
        this.id_petugas = id_petugas;
    }

    public String getNama_petugas() {
        return nama_petugas;
    }

    public void setNama_petugas(String nama_petugas) {
        this.nama_petugas = nama_petugas;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getId_level() {
        return id_level;
    }

    public void setId_level(int id_level) {
        this.id_level = id_level;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdate_at() {
        return update_at;
    }

    public void setUpdate_at(String update_at) {
        this.update_at = update_at;
    }

    public int getJumlah_lelang() {
        return jumlah_lelang;
    }

    public void setJumlah_lelang(int jumlah_lelang) {
        this.jumlah_lelang = jumlah_lelang;
    }
}
