package com.reihan.digitalauction.Models.ResponseDataModels;

import com.reihan.digitalauction.Models.DataModels.UserDataModel;

public class ResponseUserRegisterModel {

    private String msg;
    private int code;
    private UserDataModel user;

    public ResponseUserRegisterModel(String msg, int code, UserDataModel user) {
        this.msg = msg;
        this.code = code;
        this.user = user;
    }


    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public UserDataModel getUser() {
        return user;
    }

    public void setUser(UserDataModel user) {
        this.user = user;
    }

}
