package com.reihan.digitalauction.Models.ResponseDataModels;

import com.reihan.digitalauction.Models.DataModels.UsersAuctionHistoryDataModel;

import java.util.List;

public class ResponseUsersAuctionHistoryModel {

    private String msg;
    private int code;
    private List<UsersAuctionHistoryDataModel> history;

    public ResponseUsersAuctionHistoryModel(String msg, int code, List<UsersAuctionHistoryDataModel> history) {
        this.msg = msg;
        this.code = code;
        this.history = history;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public List<UsersAuctionHistoryDataModel> getHistory() {
        return history;
    }

    public void setHistory(List<UsersAuctionHistoryDataModel> history) {
        this.history = history;
    }
}
