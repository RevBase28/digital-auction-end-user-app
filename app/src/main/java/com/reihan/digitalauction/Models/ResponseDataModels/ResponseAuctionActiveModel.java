package com.reihan.digitalauction.Models.ResponseDataModels;

import com.reihan.digitalauction.Models.DataModels.AuctionActiveDataModel;

import java.util.List;

public class ResponseAuctionActiveModel {

    private String msg;
    private int code;
    private List<AuctionActiveDataModel> lelang;

    public ResponseAuctionActiveModel(String msg, int code, List<AuctionActiveDataModel> lelang) {
        this.msg = msg;
        this.code = code;
        this.lelang = lelang;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public List<AuctionActiveDataModel> getLelang() {
        return lelang;
    }

    public void setLelang(List<AuctionActiveDataModel> lelang) {
        this.lelang = lelang;
    }
}
