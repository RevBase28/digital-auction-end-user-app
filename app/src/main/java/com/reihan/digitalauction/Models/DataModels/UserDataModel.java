package com.reihan.digitalauction.Models.DataModels;

import com.google.gson.annotations.SerializedName;

public class UserDataModel {

    @SerializedName("id_user")
    private int id;

    private String nama_lengkap;
    private String username;
    private String telp;
    private String created_at;
    private String updates_at;

    public UserDataModel(int id, String nama_lengkap, String username, String telp, String created_at, String updates_at) {
        this.id = id;
        this.nama_lengkap = nama_lengkap;
        this.username = username;
        this.telp = telp;
        this.created_at = created_at;
        this.updates_at = updates_at;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNama_lengkap() {
        return nama_lengkap;
    }

    public void setNama_lengkap(String nama_lengkap) {
        this.nama_lengkap = nama_lengkap;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getTelp() {
        return telp;
    }

    public void setTelp(String telp) {
        this.telp = telp;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdates_at() {
        return updates_at;
    }

    public void setUpdates_at(String updates_at) {
        this.updates_at = updates_at;
    }
}
