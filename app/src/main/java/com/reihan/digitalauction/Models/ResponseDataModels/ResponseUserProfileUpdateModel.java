package com.reihan.digitalauction.Models.ResponseDataModels;

import com.reihan.digitalauction.Models.DataModels.UserDataModel;

public class ResponseUserProfileUpdateModel {

    private String msg;
    private int code;
    private UserDataModel profile;

    public ResponseUserProfileUpdateModel(String msg, int code, UserDataModel profile) {
        this.msg = msg;
        this.code = code;
        this.profile = profile;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public UserDataModel getProfile() {
        return profile;
    }

    public void setProfile(UserDataModel profile) {
        this.profile = profile;
    }
}
