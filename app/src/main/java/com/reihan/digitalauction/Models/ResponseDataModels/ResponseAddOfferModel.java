package com.reihan.digitalauction.Models.ResponseDataModels;

import com.reihan.digitalauction.Models.DataModels.AuctionDataModel;

public class ResponseAddOfferModel {

    private String msg;
    private int code;
    private AuctionDataModel lelang;

    public ResponseAddOfferModel(String msg, int code, AuctionDataModel lelang) {
        this.msg = msg;
        this.code = code;
        this.lelang = lelang;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public AuctionDataModel getLelang() {
        return lelang;
    }

    public void setLelang(AuctionDataModel lelang) {
        this.lelang = lelang;
    }
}
