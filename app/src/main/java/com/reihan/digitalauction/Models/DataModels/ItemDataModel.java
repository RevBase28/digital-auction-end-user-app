package com.reihan.digitalauction.Models.DataModels;

import java.util.List;

public class ItemDataModel {

    private int id_barang;
    private String nama_barang;
    private String tgl;
    private int harga_awal;
    private String deskripsi_barang;
    private String created_at;
    private String updated_at;
    private String status;
    private List<PhotoDataModel> photos;

    public ItemDataModel(int id_barang, String nama_barang, String tgl, int harga_awal, String deskripsi_barang, String created_at, String updated_at, String status, List<PhotoDataModel> photos) {
        this.id_barang = id_barang;
        this.nama_barang = nama_barang;
        this.tgl = tgl;
        this.harga_awal = harga_awal;
        this.deskripsi_barang = deskripsi_barang;
        this.created_at = created_at;
        this.updated_at = updated_at;
        this.status = status;
        this.photos = photos;
    }

    public int getId_barang() {
        return id_barang;
    }

    public void setId_barang(int id_barang) {
        this.id_barang = id_barang;
    }

    public String getNama_barang() {
        return nama_barang;
    }

    public void setNama_barang(String nama_barang) {
        this.nama_barang = nama_barang;
    }

    public String getTgl() {
        return tgl;
    }

    public void setTgl(String tgl) {
        this.tgl = tgl;
    }

    public int getHarga_awal() {
        return harga_awal;
    }

    public void setHarga_awal(int harga_awal) {
        this.harga_awal = harga_awal;
    }

    public String getDeskripsi_barang() {
        return deskripsi_barang;
    }

    public void setDeskripsi_barang(String deskripsi_barang) {
        this.deskripsi_barang = deskripsi_barang;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<PhotoDataModel> getPhotos() {
        return photos;
    }

    public void setPhotos(List<PhotoDataModel> photos) {
        this.photos = photos;
    }
}
