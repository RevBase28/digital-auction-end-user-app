package com.reihan.digitalauction.Models.ResponseDataModels;

import com.reihan.digitalauction.Models.DataModels.AuctionActiveDataModel;

import java.util.List;

public class ResponseAuctionSearchModel {

    private String msg;
    private int code;
    private List<AuctionActiveDataModel> results;

    public ResponseAuctionSearchModel(String msg, int code, List<AuctionActiveDataModel> results) {
        this.msg = msg;
        this.code = code;
        this.results = results;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public List<AuctionActiveDataModel> getResults() {
        return results;
    }

    public void setResults(List<AuctionActiveDataModel> results) {
        this.results = results;
    }
}
