package com.reihan.digitalauction.Models.DataModels;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AuctionActiveDataModel {

    private int id_lelang;
    private int id_barang;
    private String tgl_lelang;
    private String end_lelang;
    private int harga_akhir;
    private int id_user;
    private int id_petugas;
    private String status;
    private String created_at;
    private String updated_at;
    private String nama_barang;
    private String deskripsi_barang;

    @SerializedName("photos")
    private List<PhotoDataModel> photoData;

    public AuctionActiveDataModel(int id_lelang, int id_barang, String tgl_lelang, String end_lelang, int harga_akhir, int id_user, int id_petugas, String status, String created_at, String updated_at, String nama_barang, String deskripsi_barang, List<PhotoDataModel> photoData) {
        this.id_lelang = id_lelang;
        this.id_barang = id_barang;
        this.tgl_lelang = tgl_lelang;
        this.end_lelang = end_lelang;
        this.harga_akhir = harga_akhir;
        this.id_user = id_user;
        this.id_petugas = id_petugas;
        this.status = status;
        this.created_at = created_at;
        this.updated_at = updated_at;
        this.nama_barang = nama_barang;
        this.deskripsi_barang = deskripsi_barang;
        this.photoData = photoData;
    }

    public int getId_lelang() {
        return id_lelang;
    }

    public void setId_lelang(int id_lelang) {
        this.id_lelang = id_lelang;
    }

    public int getId_barang() {
        return id_barang;
    }

    public void setId_barang(int id_barang) {
        this.id_barang = id_barang;
    }

    public String getTgl_lelang() {
        return tgl_lelang;
    }

    public void setTgl_lelang(String tgl_lelang) {
        this.tgl_lelang = tgl_lelang;
    }

    public String getEnd_lelang() {
        return end_lelang;
    }

    public void setEnd_lelang(String end_lelang) {
        this.end_lelang = end_lelang;
    }

    public int getHarga_akhir() {
        return harga_akhir;
    }

    public void setHarga_akhir(int harga_akhir) {
        this.harga_akhir = harga_akhir;
    }

    public int getId_user() {
        return id_user;
    }

    public void setId_user(int id_user) {
        this.id_user = id_user;
    }

    public int getId_petugas() {
        return id_petugas;
    }

    public void setId_petugas(int id_petugas) {
        this.id_petugas = id_petugas;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getNama_barang() {
        return nama_barang;
    }

    public void setNama_barang(String nama_barang) {
        this.nama_barang = nama_barang;
    }

    public String getDeskripsi_barang() {
        return deskripsi_barang;
    }

    public void setDeskripsi_barang(String deskripsi_barang) {
        this.deskripsi_barang = deskripsi_barang;
    }

    public List<PhotoDataModel> getPhotoData() {
        return photoData;
    }

    public void setPhotoData(List<PhotoDataModel> photoData) {
        this.photoData = photoData;
    }
}
