package com.reihan.digitalauction.Models.DataModels;

public class PhotoDataModel{

    private int id_photo;
    private String file_path;
    private int id_barang;
    private String created_at;
    private String updated_at;

    public PhotoDataModel(int id_photo, String file_path, int id_barang, String created_at, String updated_at) {
        this.id_photo = id_photo;
        this.file_path = file_path;
        this.id_barang = id_barang;
        this.created_at = created_at;
        this.updated_at = updated_at;
    }

    public int getId_photo() {
        return id_photo;
    }

    public void setId_photo(int id_photo) {
        this.id_photo = id_photo;
    }

    public String getFile_path() {
        return file_path;
    }

    public void setFile_path(String file_path) {
        this.file_path = file_path;
    }

    public int getId_barang() {
        return id_barang;
    }

    public void setId_barang(int id_barang) {
        this.id_barang = id_barang;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

}
