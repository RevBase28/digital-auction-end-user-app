package com.reihan.digitalauction.Models.DataModels;

public class UserProfileDataModel {

    private int id_user;
    private String nama_lengkap;
    private String username;
    private String telp;
    private String created_at;
    private String updated_at;
    private int total_lelang;
    private int lelang_won;
    private int total_penawaran;

    public UserProfileDataModel(int id_user, String nama_lengkap, String username, String telp, String created_at, String updated_at, int total_lelang, int lelang_won, int total_penawaran) {
        this.id_user = id_user;
        this.nama_lengkap = nama_lengkap;
        this.username = username;
        this.telp = telp;
        this.created_at = created_at;
        this.updated_at = updated_at;
        this.total_lelang = total_lelang;
        this.lelang_won = lelang_won;
        this.total_penawaran = total_penawaran;
    }

    public int getId_user() {
        return id_user;
    }

    public void setId_user(int id_user) {
        this.id_user = id_user;
    }

    public String getNama_lengkap() {
        return nama_lengkap;
    }

    public void setNama_lengkap(String nama_lengkap) {
        this.nama_lengkap = nama_lengkap;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getTelp() {
        return telp;
    }

    public void setTelp(String telp) {
        this.telp = telp;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public int getTotal_lelang() {
        return total_lelang;
    }

    public void setTotal_lelang(int total_lelang) {
        this.total_lelang = total_lelang;
    }

    public int getLelang_won() {
        return lelang_won;
    }

    public void setLelang_won(int lelang_won) {
        this.lelang_won = lelang_won;
    }

    public int getTotal_penawaran() {
        return total_penawaran;
    }

    public void setTotal_penawaran(int total_penawaran) {
        this.total_penawaran = total_penawaran;
    }
}
