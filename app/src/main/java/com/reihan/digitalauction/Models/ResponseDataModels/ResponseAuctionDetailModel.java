package com.reihan.digitalauction.Models.ResponseDataModels;


import com.reihan.digitalauction.Models.DataModels.AuctionDetailDataModel;

public class ResponseAuctionDetailModel {

    private String msg;
    private int code;
    private AuctionDetailDataModel lelang;

    public ResponseAuctionDetailModel(String msg, int code, AuctionDetailDataModel lelang) {
        this.msg = msg;
        this.code = code;
        this.lelang = lelang;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public AuctionDetailDataModel getLelang() {
        return lelang;
    }

    public void setLelang(AuctionDetailDataModel lelang) {
        this.lelang = lelang;
    }
}
