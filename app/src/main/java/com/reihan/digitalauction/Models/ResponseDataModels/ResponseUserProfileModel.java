package com.reihan.digitalauction.Models.ResponseDataModels;


import com.reihan.digitalauction.Models.DataModels.UserProfileDataModel;

public class ResponseUserProfileModel {

    private String msg;
    private int code;
    private UserProfileDataModel profile;

    public ResponseUserProfileModel(String msg, int code, UserProfileDataModel profile) {
        this.msg = msg;
        this.code = code;
        this.profile = profile;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public UserProfileDataModel getProfile() {
        return profile;
    }

    public void setProfile(UserProfileDataModel profile) {
        this.profile = profile;
    }
}
