package com.reihan.digitalauction.Models.ResponseDataModels;

import com.reihan.digitalauction.Models.DataModels.AuctionActiveDataModel;

import java.util.List;

public class ResponseOnBidAuctionModel {

    private String msg;
    private int code;
    private List<AuctionActiveDataModel> onBid;

    public ResponseOnBidAuctionModel(String msg, int code, List<AuctionActiveDataModel> onBid) {
        this.msg = msg;
        this.code = code;
        this.onBid = onBid;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public List<AuctionActiveDataModel> getOnBid() {
        return onBid;
    }

    public void setOnBid(List<AuctionActiveDataModel> onBid) {
        this.onBid = onBid;
    }
}
