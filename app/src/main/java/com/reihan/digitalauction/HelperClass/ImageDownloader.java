package com.reihan.digitalauction.HelperClass;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ImageView;

import java.io.IOException;
import java.io.InputStream;

public class ImageDownloader extends AsyncTask<String, Void, Bitmap> {

    private ImageView itemImage;


    public ImageDownloader(){}
    public ImageDownloader(ImageView itemImage) {
        this.itemImage = itemImage;
    }

    @Override
    protected Bitmap doInBackground(String... urls) {

        String url = urls[0];
        Bitmap imageItem = null;

        try {
            InputStream in = new java.net.URL(url).openStream();
            imageItem = BitmapFactory.decodeStream(in);
        } catch (IOException e) {
            e.printStackTrace();
        }

        Log.d("Image Downloader", "Decode : " + imageItem.toString());

        return imageItem;
    }

    @Override
    protected void onPostExecute(Bitmap bitmap) {
        super.onPostExecute(bitmap);

        if(itemImage != null){
            Log.d("Image Downloader", bitmap.toString());
            itemImage.setImageBitmap(bitmap);
        }
    }

}
