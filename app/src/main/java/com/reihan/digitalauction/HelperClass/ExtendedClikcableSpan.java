package com.reihan.digitalauction.HelperClass;

import android.content.Context;
import android.content.Intent;
import android.text.TextPaint;
import android.text.style.ClickableSpan;
import android.view.View;

import androidx.annotation.NonNull;

public class ExtendedClikcableSpan extends ClickableSpan {

    private String text;
    private Context context;
    private Class <?> classTo;

    public ExtendedClikcableSpan(String text, Context context, Class<?> classTo) {
        this.text = text;
        this.context = context;
        this.classTo = classTo;
    }

    @Override
    public void onClick(@NonNull View view) {
        Intent i = new Intent(context,classTo);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(i);
    }

    public void updateDrawState(TextPaint ds) {
        ds.setUnderlineText(false); // set to false to remove underline
    }

}
