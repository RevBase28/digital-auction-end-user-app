package com.reihan.digitalauction.HelperClass;

import android.app.Activity;
import android.graphics.Typeface;

import com.norbsoft.typefacehelper.TypefaceCollection;
import com.norbsoft.typefacehelper.TypefaceHelper;

public class TypefaceManager {

    private Activity activity;
    public TypefaceCollection fivo_bold_oblique, fivo_light_oblique, fivo;

    public TypefaceManager(Activity activity) {
        this.activity = activity;
    }

    public void setTypeface(){

        fivo = new TypefaceCollection.Builder()
                .set(Typeface.NORMAL, Typeface.createFromAsset(activity.getAssets(), "fonts/fivoSansRegular.otf"))
                .set(Typeface.BOLD, Typeface.createFromAsset(activity.getAssets(), "fonts/fivoSansMedium.otf"))
                .create();

        fivo_bold_oblique = new TypefaceCollection.Builder()
                .set(Typeface.NORMAL, Typeface.createFromAsset(activity.getAssets(), "fonts/fivoSansBoldOblique.otf"))
                .create();

        fivo_light_oblique = new TypefaceCollection.Builder()
                .set(Typeface.NORMAL, Typeface.createFromAsset(activity.getAssets(), "fonts/fivoSansLightOblique.otf"))
                .create();

        TypefaceHelper.init(fivo);
        TypefaceHelper.typeface(activity);
    }

}
