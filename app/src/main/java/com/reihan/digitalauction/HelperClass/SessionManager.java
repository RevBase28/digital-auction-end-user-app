package com.reihan.digitalauction.HelperClass;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.List;

public class SessionManager {

    public static final String SESSION = "session";

    public static final String SESSION_USER_ID = "user_id";
    public static final String SESSION_USER_NAME = "user_name";

    public static final String SESSION_LOGIN = "login";

    public SharedPreferences preferences;
    public SharedPreferences.Editor editor;

    public SessionManager(Context context) {
        preferences = context.getSharedPreferences(SESSION, Context.MODE_PRIVATE);
        editor = preferences.edit();
    }

    public void saveSessionUserName(String value){
        editor.putString(SESSION_USER_NAME, value);
        editor.commit();
    }

    public void saveSessionUserId(int value){
        editor.putInt(SESSION_USER_ID, value);
        editor.commit();
    }

    public void saveSessionLogin(Boolean value){
        editor.putBoolean(SESSION_LOGIN, value);
        editor.commit();
    }

    public String getSessionUserName(){
        return preferences.getString(SESSION_USER_NAME, "");
    }

    public int getSessionUserId(){
        return preferences.getInt(SESSION_USER_ID, 0);
    }

    public Boolean getSessionLogin(){
        return preferences.getBoolean(SESSION_LOGIN, false);
    }
}
