package com.reihan.digitalauction.HttpCommunication;

import com.reihan.digitalauction.Models.ResponseDataModels.ResponseAddOfferModel;
import com.reihan.digitalauction.Models.ResponseDataModels.ResponseAuctionActiveModel;
import com.reihan.digitalauction.Models.ResponseDataModels.ResponseAuctionDetailModel;
import com.reihan.digitalauction.Models.ResponseDataModels.ResponseAuctionSearchModel;
import com.reihan.digitalauction.Models.ResponseDataModels.ResponseChangePasswordModel;
import com.reihan.digitalauction.Models.ResponseDataModels.ResponseOnBidAuctionModel;
import com.reihan.digitalauction.Models.ResponseDataModels.ResponseUserLoginModel;
import com.reihan.digitalauction.Models.ResponseDataModels.ResponseUserProfileModel;
import com.reihan.digitalauction.Models.ResponseDataModels.ResponseUserProfileUpdateModel;
import com.reihan.digitalauction.Models.ResponseDataModels.ResponseUserRegisterModel;
import com.reihan.digitalauction.Models.ResponseDataModels.ResponseUsersAuctionHistoryModel;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.PATCH;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface ApiInterface {
    @FormUrlEncoded
    @POST("masyarakat/login")
    Call<ResponseUserLoginModel> userLogin(
            @Field("username") String username,
            @Field("password") String password
    );

    @FormUrlEncoded
    @POST("masyarakat/register")
    Call<ResponseUserRegisterModel> userRegister(
            @Field("nama_lengkap") String fullName,
            @Field("username") String username,
            @Field("password") String password,
            @Field("telp") String telp
    );

    @GET("masyarakat/profile/{id}")
    Call<ResponseUserProfileModel> getUserProfile(
            @Path("id") int id
    );

    @FormUrlEncoded
    @PUT("masyarakat/profile/edit/{id}")
    Call<ResponseUserProfileUpdateModel> updateProfile(
            @Path("id") int id,
            @Field("nama_lengkap") String fullName,
            @Field("username") String username,
            @Field("telp") String telp
    );

    @FormUrlEncoded
    @PATCH("masyarakat/profile/changePass/{id}")
    Call<ResponseChangePasswordModel> changePassword(
            @Path("id") int id,
            @Field("old_password") String oldPassword,
            @Field("new_password") String new_password
    );

    @GET("lelang")
    Call<ResponseAuctionActiveModel> getActiveAuction();

    @GET("lelang/{id}")
    Call<ResponseAuctionDetailModel> getDetailAuction(
            @Path("id") int id
    );

    @FormUrlEncoded
    @PATCH("lelang/addoffer/{id}")
    Call<ResponseAddOfferModel> addOffer(
            @Path("id") int id,
            @Field("harga_akhir") int bidPrice,
            @Field("id_user") int id_user
    );

    @GET("lelang/onBid/{id}")
    Call<ResponseOnBidAuctionModel> getOnBidAuction(
            @Path("id") int id
    );

    @GET("lelang/history/{id}")
    Call<ResponseUsersAuctionHistoryModel> getUsersHistory(
            @Path("id") int id
    );

    @FormUrlEncoded
    @POST("lelang/search")
    Call<ResponseAuctionSearchModel> searchAuction(
            @Field("query") String query
    );
}
