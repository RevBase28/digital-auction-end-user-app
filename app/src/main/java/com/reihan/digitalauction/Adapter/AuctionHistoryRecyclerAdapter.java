package com.reihan.digitalauction.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.norbsoft.typefacehelper.TypefaceHelper;
import com.reihan.digitalauction.HelperClass.ImageDownloader;
import com.reihan.digitalauction.Models.DataModels.UsersAuctionHistoryDataModel;
import com.reihan.digitalauction.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class AuctionHistoryRecyclerAdapter extends RecyclerView.Adapter<AuctionHistoryRecyclerAdapter.ItemViewHolder> {
    
    private List<UsersAuctionHistoryDataModel> data;
    private Context context;
    private int itemLayout;
    
    private String BASE_IMAGE_URL = "http://192.168.43.60:8000/barangs/image/";

    public AuctionHistoryRecyclerAdapter(List<UsersAuctionHistoryDataModel> data, Context context, int itemLayout) {
        this.data = data;
        this.context = context;
        this.itemLayout = itemLayout;
    }

    public static class ItemViewHolder extends RecyclerView.ViewHolder{
        TextView itemName, itemDesc, bidDate, bidStatus;
        ImageView itemImage;
        CardView itemContainer;

        public ItemViewHolder(@NonNull View itemView) {
            super(itemView);

            itemName = itemView.findViewById(R.id.item_name);
            itemDesc = itemView.findViewById(R.id.item_desc);
            itemImage = itemView.findViewById(R.id.item_image);
            bidDate = itemView.findViewById(R.id.bid_date);
            bidStatus = itemView.findViewById(R.id.bid_status);
            itemContainer = itemView.findViewById(R.id.item_container);

            TypefaceHelper.typeface(itemView);
        }
    }

    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(itemLayout, parent, false);
        return new ItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder holder, int position) {

        holder.itemName.setText(data.get(position).getNama_barang());
        holder.itemDesc.setText(data.get(position).getDeskripsi_barang());
        holder.bidStatus.setText(data.get(position).getBidStatus());

        if(data.get(position).getBidStatus().equals("Win")){
            holder.bidStatus.setTextColor(context.getResources().getColor(R.color.green));
        }

        try {
            holder.bidDate.setText(setDate(data.get(position).getTgl_lelang()));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if(data.get(position).getPhotos().toArray().length > 0 ){
            new ImageDownloader(holder.itemImage).execute(BASE_IMAGE_URL + data.get(position).getPhotos().get(0).getFile_path());
        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    private String setDate(String endAuction) throws ParseException {

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date parsedDate = dateFormat.parse(endAuction);

        dateFormat = new SimpleDateFormat("E, dd MMM yyyy");
        String date = dateFormat.format(parsedDate);

        return date;
    }
}
