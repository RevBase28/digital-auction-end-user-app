package com.reihan.digitalauction.Adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.reihan.digitalauction.HelperClass.ImageDownloader;
import com.reihan.digitalauction.Models.DataModels.PhotoDataModel;
import com.reihan.digitalauction.R;
import com.smarteist.autoimageslider.SliderViewAdapter;

import java.util.List;

public class ImageSliderAdapter extends SliderViewAdapter<ImageSliderAdapter.ViewHolder> {

    private Context context;
    private List<PhotoDataModel> photoData;
    private List<Bitmap> images;

    private String BASE_IMAGE_URL = "http://192.168.43.60:8000/barangs/image/";

    public ImageSliderAdapter(Context context, List<PhotoDataModel> photoData) {
        this.context = context;
        this.photoData = photoData;
    }

    public static class ViewHolder extends SliderViewAdapter.ViewHolder{

        ImageView imageItem;

        public ViewHolder(View itemView) {
            super(itemView);
            imageItem = itemView.findViewById(R.id.imageItem);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent) {
        View inflate = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_image_slider, null);
        return new ViewHolder(inflate);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        new ImageDownloader(viewHolder.imageItem).execute(BASE_IMAGE_URL + photoData.get(position).getFile_path());
    }

    @Override
    public int getCount() {
        return photoData.size();
    }
}
