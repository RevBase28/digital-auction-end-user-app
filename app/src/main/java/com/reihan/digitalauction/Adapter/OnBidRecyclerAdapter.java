package com.reihan.digitalauction.Adapter;

import android.content.Context;
import android.content.Intent;
import android.os.CountDownTimer;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.norbsoft.typefacehelper.TypefaceHelper;
import com.reihan.digitalauction.Activities.AuctionDetailActivity;
import com.reihan.digitalauction.HelperClass.ImageDownloader;
import com.reihan.digitalauction.Models.DataModels.AuctionActiveDataModel;
import com.reihan.digitalauction.R;

import java.sql.Timestamp;
import java.util.List;

public class OnBidRecyclerAdapter extends RecyclerView.Adapter<OnBidRecyclerAdapter.ItemViewHolder> {

    private List<AuctionActiveDataModel> lelangData;
    private Context context;
    private int itemLayout, userId;
    private String BASE_IMAGE_URL = "http://192.168.43.60:8000/barangs/image/";

    public OnBidRecyclerAdapter(List<AuctionActiveDataModel> lelangData, Context context, int itemLayout, int userId) {
        this.lelangData = lelangData;
        this.context = context;
        this.itemLayout = itemLayout;
        this.userId = userId;
    }

    public static class ItemViewHolder extends RecyclerView.ViewHolder{
        TextView itemName, itemDesc, price, countdown, status;
        ImageView itemImage;
        CardView itemContainer;

        public ItemViewHolder(@NonNull View itemView) {
            super(itemView);

            itemName = itemView.findViewById(R.id.item_name);
            itemDesc = itemView.findViewById(R.id.item_desc);
            itemImage = itemView.findViewById(R.id.item_image);
            price = itemView.findViewById(R.id.highestPrice);
            status = itemView.findViewById(R.id.bid_status);
            countdown = itemView.findViewById(R.id.countdown);
            itemContainer = itemView.findViewById(R.id.item_container);

            TypefaceHelper.typeface(itemView);

        }
    }

    @NonNull
    @Override
    public OnBidRecyclerAdapter.ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(itemLayout, parent, false);
        return new OnBidRecyclerAdapter.ItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull OnBidRecyclerAdapter.ItemViewHolder holder, int position) {

        long startAuctionMillis,endAuctionMillis,totalMillis;

        if(lelangData.get(position).getEnd_lelang() !=  null){
            Timestamp endAuction = Timestamp.valueOf(lelangData.get(position).getEnd_lelang());
            startAuctionMillis = System.currentTimeMillis();
            endAuctionMillis = endAuction.getTime();
            totalMillis = endAuctionMillis - startAuctionMillis;

            CountDownTimer countDownTimer = new CountDownTimer(totalMillis, 1000) {
                @Override
                public void onTick(long timeLeftMillis) {
                    long second = (timeLeftMillis/ 1000) % 60;
                    long minutes =((timeLeftMillis/ (1000*60)) % 60);
                    long hours =((timeLeftMillis / (1000*60*60)) % 24);
                    long days = ((timeLeftMillis / (1000*60*60*24)) % 24);

                    if(days > 0){
                        holder.countdown.setText(String.valueOf(days) + "d "+ String.format("%02dh %02dm",hours,minutes));
                    }else{
                        holder.countdown.setText(String.format("%02dh %02dm %02ds",hours,minutes,second));
                    }
                }

                @Override
                public void onFinish() {
                    holder.countdown.setText("Finish");
                    holder.itemContainer.setEnabled(false);
                }
            }.start();
        }else{
            holder.countdown.setText("Indefinetly");
        }

        holder.itemName.setText(lelangData.get(position).getNama_barang());
        holder.itemDesc.setText(lelangData.get(position).getDeskripsi_barang());
        holder.price.setText("Rp. "+String.valueOf(lelangData.get(position).getHarga_akhir()));

        if(lelangData.get(position).getId_user() == userId){
            holder.status.setText("Highest Bidder");
            holder.status.setTextColor(context.getResources().getColor(R.color.green));
        }else{
            holder.status.setText("Bid Taken");
        }

        if(lelangData.get(position).getPhotoData().toArray().length > 0 ){
            new ImageDownloader(holder.itemImage).execute(BASE_IMAGE_URL + lelangData.get(position).getPhotoData().get(0).getFile_path());
        }

        holder.itemContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(context.getApplicationContext(), AuctionDetailActivity.class);
                i.putExtra("id", lelangData.get(position).getId_lelang());
                context.startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return lelangData.size();
    }
}
